import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LandingComponent} from './landing/landing.component';
import {TeamComponent} from './team/team.component';
import {PlayerComponent} from './player/player.component';

const PagesRoutes = [
  {path: '', component: LandingComponent, pathMatch: 'full'},
  {path: 'team/:team_id', component: TeamComponent},
  {path: 'team/:team_id/player/:player_id', component: PlayerComponent}
];


@NgModule({
  imports: [
    RouterModule.forRoot(PagesRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
