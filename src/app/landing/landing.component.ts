import {Component, OnInit} from '@angular/core';
import {TeamService} from '../team.service';
import {Observable} from 'rxjs';
import {Team} from "../models/team";

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {

  teams: Observable<Array<Team>>;
  displayedColumns = ['id', 'name', 'flag'];

  constructor(private teService: TeamService) {
  }

  ngOnInit() {
    this.teams = this.teService.getTeamList();
  }

}
