/**
 * Created by garusis on 17/06/18.
 */
export class Player {
  id: number;
  fullname: string;
  no: string;
  date_of_birth: Date;
  place_of_birth: string;
  height: string;
  position: string;
  club: string;
  goals: string;
  team_id: number;
  created_at: Date;
  updated_at: Date;

  constructor() {
  }
}
