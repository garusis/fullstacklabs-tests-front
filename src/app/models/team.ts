/**
 * Created by garusis on 17/06/18.
 */
export class Team {
  id: number;
  name: string;
  coach: string;
  flag: string;
  created_at: Date;
  updated_at: Date;

  constructor() {
  }
}
