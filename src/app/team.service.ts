import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../environments/environment';
import {Observable} from 'rxjs';
import {Team} from "./models/team";

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  private servicePath = `${environment.apiUrl}/teams`

  constructor(private http: HttpClient) {
  }


  getTeamList(): Observable<Array<Team>> {
    return this.http.get<Team[]>(this.servicePath);
  }

  getTeamDetails(id): Observable<Team> {
    return this.http.get<Team>(`${this.servicePath}/${id}`);
  }

}
