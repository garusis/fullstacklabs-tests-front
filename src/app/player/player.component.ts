import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {PlayerService} from "../player.service";
import {Player} from "../models/player";

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit {

  private player: Player = new Player();

  constructor(private route: ActivatedRoute, private playerService: PlayerService) {
  }

  ngOnInit() {
    const paramMap = this.route.snapshot.paramMap
    this.loadTeam(paramMap.get('team_id'), paramMap.get("player_id"));
  }


  loadTeam(team_id, player_id) {
    this.playerService.getPlayerDetails(team_id, player_id)
      .subscribe(player => {
        this.player = player;
      });
  }

}
