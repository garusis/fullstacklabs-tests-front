import {Injectable} from '@angular/core';
import {environment} from "../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/index";
import {Team} from "./models/team";
import {Player} from "./models/player";

@Injectable({
  providedIn: 'root'
})
export class PlayerService {
  private servicePath = `${environment.apiUrl}/teams`

  constructor(private http: HttpClient) {
  }

  getPlayerList(team_id): Observable<Array<Player>> {
    return this.http.get<Player[]>(`${this.servicePath}/${team_id}/players`);
  }

  getPlayerDetails(team_id, player_id): Observable<Player> {
    return this.http.get<Player>(`${this.servicePath}/${team_id}/players/${player_id}`);
  }
}
