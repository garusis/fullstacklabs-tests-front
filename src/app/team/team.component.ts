import {Component, OnInit} from '@angular/core';
import {TeamService} from "../team.service";
import {Observable} from "rxjs/index";
import {ActivatedRoute} from "@angular/router";
import {Team} from "../models/team";
import {PlayerService} from "../player.service";
import {Player} from "../models/player";

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit {

  private team: Team = new Team();
  private players: Observable<Array<Player>>;
  displayedColumns = ['id', 'fullname', 'no', 'position'];

  constructor(private route: ActivatedRoute, private teService: TeamService, private playerService: PlayerService) {
  }

  ngOnInit() {
    this.loadTeam(this.route.snapshot.paramMap.get('team_id'));
  }

  loadTeam(id) {
    this.teService.getTeamDetails(id)
      .subscribe(team => {
        this.team = team
        this.players = this.playerService.getPlayerList(id);
      });
  }

}
