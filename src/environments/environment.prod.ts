export const environment = {
  production: true,
  apiUrl: 'https://world-cup-test.herokuapp.com/api/v1/'
};
