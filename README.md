# FullstacklabsTestFront

This repository is a test for Fullstacklabs consuming an REST API since an Angular.io client.
This client is deployed in [heroku](https://world-cup-front.herokuapp.com).


Requirements:

1. Node version v6.11.5 or greater
2. Angular CLI v 1.5.5 (npm install -g @angular/cli)

How to run:
1. Make `npm install` on the project root
2. Run `ng serve`
7. Go to the browser and enter url *localhost:4200'*
